import time
import threading
from fastapi import FastAPI
from src.view.deputado import rotas as deputado
from src.view.atualizacao_api import rotas as atualizacoes
from src.view.despesa import rotas as despesa
import src.controller.atualizacao_api as atualizacao
import datetime

app = FastAPI()
api_prefix = "/api/v1/"

@app.get(api_prefix)
def home():
    return {
        "status": True,
        "message": "Api em funcionamento"
    }


deputado(app=app, prefixo=api_prefix)
atualizacoes(app=app, prefixo=api_prefix)
despesa(app=app, prefixo=api_prefix)
def rodar_background():
    proximo = None
    while True:
        agora = datetime.datetime.utcnow()
        if proximo is None:
            atualizacao.main()
            atualizacao.main_despesas()
            proximo = agora + datetime.timedelta(weeks=5)
            continue
        elif proximo.date() == agora.date():
            atualizacao.main()
            atualizacao.main_despesas()
            proximo = agora + datetime.timedelta(weeks=5)
        time.sleep((proximo - agora).total_seconds())
background = threading.Thread(target=rodar_background)
background.start()



