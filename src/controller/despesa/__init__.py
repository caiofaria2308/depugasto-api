from psycopg2.extras import RealDictCursor
import src.sql_app as database


def get_all(pagina=1, quantidade_pagina=20, ano=2021):
    con = database.conectar()
    if not con[0]:
        return {
            "status": False,
            "message": "Erro ao conectar com banco de dados! Tente novamente mais tarde"
        }
    con = con[1]
    retorno = {
        "status": False,
        "data": [],
        "message": ""
    }
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        depois = quantidade_pagina * (pagina - 1)
        sql = f'''
            SELECT
                d._id as id,
                d.deputado_id,
                d.ano,
                d.mes,
                d.tipo_despesa,
                d.tipo_documento,
                d.documento,
                d.valor_documento,
                d.url_documento,
                d.fornecedor_cnpjcpf,
                d.fornecedor_nome,
                d.valor_liquido,
                d.valor_glosa,
                d.codigo_lote,
                to_char(d.data_criacao, 'dd/mm/YYYY hh:MM:ss') as data_criacao,
                to_char(d.data_atualizacao, 'dd/mm/YYYY hh:MM:ss') as data_atualizacao,
                (select count(_id) from despesas d where d.ano = {ano}) - 1 as quantidade_total,
             (select ceil(count(_id) / {float(quantidade_pagina)}) from despesas d where d.ano = {ano}) as quantidade_pagina
            FROM despesas d
            where d.ano = {ano}
            order by d.ano desc, d.mes desc, d.tipo_despesa
            limit {quantidade_pagina} offset {depois}
        '''
        try:
            cursor.execute(sql)
            retorno["status"] = True
            retorno["data"] = cursor.fetchall()
            retorno["pagina"] = pagina
            retorno["quantidade_por_pagina"] = quantidade_pagina
            if len(retorno["data"]) != 0:
                retorno["quantidade_total"] = retorno["data"][0]["quantidade_total"]
                retorno["quantidade_de_pagina"] = retorno["data"][0]["quantidade_pagina"]
            else:
                retorno["quantidade_total"] = 0
                retorno["quantidade_de_pagina"] = 1
        except Exception as e:
            retorno["status"] = False
            retorno["message"] = "erro ao selecionar despesas"
    return retorno


def rank_despesas(ano=None):
    con = database.conectar()
    if not con[0]:
        return {
            "status": False,
            "message": "Erro ao conectar com banco de dados! Tente novamente mais tarde"
        }
    con = con[1]
    retorno = {
        "status": False,
        "data": [],
        "message": ""
    }
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        ano_string = ""
        if ano is not None:
            ano_string = f"WHERE d.ano = {ano}"
        sql = f'''
            SELECT
                d.deputado_id,
                de.nome as deputado_nome,
                de.id_camara as deputado_id_camara,
                ROUND(sum(d.valor_liquido), 2) as total_gasto
            FROM despesas d
            LEFT JOIN deputado de ON de._id = d.deputado_id
            {ano_string}
            GROUP BY d.deputado_id, de.nome, de.id_camara
            ORDER BY total_gasto desc
            LIMIT 20
        '''
        try:
            cursor.execute(sql)
            retorno["status"] = True
            retorno["data"] = cursor.fetchall()
        except Exception as e:
            retorno["status"] = False
            retorno["message"] = "Erro ao selecionar rank de deputados"
    return retorno


def get_despesas_deputado(deputado_id, pagina=1, quantidade_pagina=20, ano=None):
    con = database.conectar()
    if not con[0]:
        return {
            "status": False,
            "message": "Erro ao conectar com banco de dados! Tente novamente mais tarde"
        }
    con = con[1]
    retorno = {
        "status": False,
        "data": [],
        "message": ""
    }
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        ano_string = ""
        if ano is not None:
            ano_string = f"and d.ano = {ano}"
        depois = quantidade_pagina * (pagina - 1)
        sql = f'''
            SELECT
                d._id as id,
                d.deputado_id,
                d.ano,
                d.mes,
                d.tipo_despesa,
                d.tipo_documento,
                d.documento,
                d.valor_documento,
                d.url_documento,
                d.fornecedor_cnpjcpf,
                d.fornecedor_nome,
                d.valor_liquido,
                d.valor_glosa,
                d.codigo_lote,
                to_char(d.data_criacao, 'dd/mm/YYYY hh:MM:ss') as data_criacao,
                to_char(d.data_atualizacao, 'dd/mm/YYYY hh:MM:ss') as data_atualizacao,
                (select count(_id) from despesas d where d.deputado_id = '{deputado_id}' {ano_string}) as quantidade_total,
                (select CEIL(count(_id) / {float(quantidade_pagina)}) from despesas d where d.deputado_id = '{deputado_id}' {ano_string}) as quantidade_pagina 
            FROM despesas d
            where d.deputado_id = '{deputado_id}' {ano_string}
            limit {quantidade_pagina} offset {depois}
        '''
        try:
            cursor.execute(sql)
            retorno["status"] = True
            retorno["data"] = cursor.fetchall()
            retorno["pagina"] = pagina
            if len(retorno["data"]) != 0:
                retorno["quantidade_total"] = retorno["data"][0]["quantidade_total"]
                retorno["quantidade_de_pagina"] = retorno["data"][0]["quantidade_pagina"]
            else:
                retorno["quantidade_total"] = 0
                retorno["quantidade_de_pagina"] = 1
        except Exception as e:
            retorno["status"] = False
            retorno["message"] = "erro ao selecionar despesas do deputado"
    return retorno


def get_rank_despesa_deputado(deputado_id: str, ano=None):
    con = database.conectar()
    if not con[0]:
        return {
            "status": False,
            "message": "Erro ao conectar com banco de dados! Tente novamente mais tarde"
        }
    con = con[1]
    retorno = {
        "status": False,
        "data": [],
        "message": ""
    }
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        ano_string = ""
        if ano is not None:
            ano_string = f"and d.ano = {ano}"
        sql = f'''
            SELECT
                d.tipo_despesa,
                ROUND(sum(valor_liquido), 2) as total_liquido,
                ROUND(sum(valor_glosa), 2) as total_glosa
            FROM despesas d
            WHERE d.deputado_id = '{deputado_id}' {ano_string}
            GROUP BY tipo_despesa
            ORDER BY total_liquido DESC
        '''
        try:
            cursor.execute(sql)
            retorno["status"] = True
            retorno["data"] = cursor.fetchall()
        except:
            retorno["status"] = False
            retorno["message"] = "Erro ao retornar rank de despesas do deputado "
    return retorno
