from psycopg2.extras import RealDictCursor

import src.sql_app as database

def get_all(pagina: int = 1, quantidade_pagina=20, uf="SP"):
    con = database.conectar()
    if not con[0]:
        return {
            "status": False,
            "message": "Erro ao conectar com banco de dados! Tente novamente mais tarde"
        }
    con = con[1]
    retorno = {
        "status": False,
        "data": [],
        "message": ""
    }
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        depois = quantidade_pagina * (pagina - 1)
        sql = f'''
            SELECT
                _id as id,
                id_camara,
                nome_civil,
                nome,
                sigla_partido,
                sigla_uf,
                email,
                gabinete_nome,
                gabinete_predio,
                gabinete_sala,
                gabinete_andar,
                gabinete_telefone,
                situacao,
                sexo,
                to_char(data_nascimento , 'dd/mm/YYYY')as data_nascimento,
                to_char(data_falecimento, 'dd/mm/YYYY') as data_falecimento,
                municipio_nascimento,
                escolaridade,
                to_char(data_criacao, 'dd/mm/YYYY hh:MM:ss') as data_criacao ,
                to_char(data_atualizacao, 'dd/mm/YYYY hh:MM:ss') as data_atualizacao,
             (select count(_id) from deputado d where d.sigla_uf = '{uf}') as quantidade_total,
             (select CEIL(count(_id) / {float(quantidade_pagina)}) from deputado d where d.sigla_uf = '{uf}') as quantidade_pagina
            FROM deputado
            WHERE sigla_uf = '{uf}'
            order by nome asc
            LIMIT {quantidade_pagina} offset {depois}
        '''
        depois = quantidade_pagina * (pagina-1)
        try:
            cursor.execute(sql)
            retorno["status"] = True
            retorno["data"] = cursor.fetchall()
            retorno["pagina"] = pagina
            retorno["quantidade_por_pagina"] = quantidade_pagina
            if len(retorno["data"]) != 0:
                retorno["quantidade_total"] = retorno["data"][0]["quantidade_total"]
                retorno["quantidade_de_pagina"] = retorno["data"][0]["quantidade_pagina"]
            else:
                retorno["quantidade_total"] = 0
                retorno["quantidade_de_pagina"] = 1

        except Exception as e:
            retorno["status"] = False
            retorno["message"] = "erro ao selecionar deputados"
    return retorno
