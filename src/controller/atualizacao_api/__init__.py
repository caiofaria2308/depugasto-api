import sys
from typing import List
import datetime
import src.common as common
from psycopg2.extras import RealDictCursor
import src.sql_app as database
import requests
import os
import dotenv
from multiprocessing import Process


def get_all(pagina: int = 1, quantidade_pagina=20):
    con = database.conectar()
    if not con[0]:
        return {
            "status": False,
            "message": "Erro ao conectar com banco de dados! Tente novamente mais tarde"
        }
    con = con[1]
    retorno = {
        "status": False,
        "data": [],
        "message": ""
    }
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        depois = quantidade_pagina * (pagina-1)
        sql = f'''
            SELECT
                _id as id,
                to_char(data_atualizacao, 'dd/mm/YYYY hh:MM:ss') as data_atualizacao,
                (select count(_id) from atualizacao_api) as quantidade_total,
                (select CEIL(count(_id) / {float(quantidade_pagina)}) from atualizacao_api) as quantidade_pagina
            FROM atualizacao_api
            LIMIT {quantidade_pagina} offset {depois}
        '''
        try:
            cursor.execute(sql)
            retorno["status"] = True
            retorno["data"] = cursor.fetchall()
            retorno["pagina"] = pagina
            retorno["quantidade_por_pagina"] = quantidade_pagina
            if len(retorno["data"]) != 0:
                retorno["quantidade_total"] = retorno["data"][0]["quantidade_total"]
                retorno["quantidade_de_pagina"] = retorno["data"][0]["quantidade_pagina"]
            else:
                retorno["quantidade_total"] = 0
                retorno["quantidade_de_pagina"] = 1

        except:
            retorno["status"] = False,
            retorno["message"] = "erro ao selecionar atualizações"
    return retorno

def verifica_processos(processos: List[Process]):
    acabou = False
    while not acabou:
        terminaram = []
        for processo in processos:
            if processo.is_alive():
                terminaram.append(False)
            else:
                terminaram.append(True)
        acabou = all(terminaram)
    for processo in processos:
        processo.kill()
    return acabou

def main():
    verifica = verifica_deputado()
    if verifica["status"]:
        if verifica["atualizar"]:
            #inserindo/atualizado deputados
            atualizar_ultima_atualizacao()
            deputados = get_deputados()
            processos = []
            qtd_deputados = len(deputados)
            for antes in range(0, qtd_deputados, 200):
                depois = antes + 199
                if depois > qtd_deputados:
                    depois = qtd_deputados - 1
                print(f"criando processo do {antes} ao {depois} deputados")
                p = Process(
                        target=salvar_deputados,
                        kwargs={
                            "deputados": deputados[antes: depois]
                        }
                    )
                p.start()
                processos.append(p)
            verifica = verifica_processos(processos)

    return verifica

def main_despesas():
    verifica = verifica_deputado()
    if verifica["status"]:
        if verifica["atualizar"]:
            processos = []
            despesas = get_despesas()
            qtd_despesas = len(despesas)
            for antes in range(0, qtd_despesas, 10000):
                depois = antes + 9999
                if depois > qtd_despesas:
                    depois = qtd_despesas - 1
                print(f"criando processo do {antes} ao {depois} despesas")
                p = Process(
                    target=salvar_despesas,
                    kwargs={
                        "despesas": despesas[antes: depois]
                    }
                )
                p.start()
                processos.append(p)
            verifica = verifica_processos(processos)
            return verifica

def atualizar_ultima_atualizacao():
    con = database.conectar()
    if not con[0]:
        return {
            "status": False,
            "message": "Erro ao conectar com banco de dados"
        }
    con = con[1]
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        cursor.execute('''
            insert into atualizacao_api values(
                uuid_generate_v4(),
                current_timestamp
            )
        ''')
        con.commit()


def verifica_deputado():
    con = database.conectar()
    if not con[0]:
        return {
            "status": False,
            "message": "Erro ao conectar com banco de dados"
        }
    con = con[1]
    retorno = {}
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        sql = '''
            select
                aa.data_atualizacao ultima_atualizacao 
            from atualizacao_api aa
            where 
                extract(month from aa.data_atualizacao) = extract(month from current_timestamp) and
                extract(year from aa.data_atualizacao) = extract(year from current_timestamp)
            order by aa.data_atualizacao desc 
            limit 1
        '''
        try:
            cursor.execute(sql)
            data = cursor.fetchall()
            retorno["status"] = True
            if len(data) == 1:
                retorno["atualizar"] = False
                if "data_atualizacao" in data[0]:
                    retorno["ultima_atualizacao"] = data[0]["data_atualizacao"]
            else:
                retorno["atualizar"] = True
        except:
            retorno["status"] = False
            retorno["message"] = "erro ao verificar data de atualização da api"
    return retorno


def get_deputados():
    dotenv.load_dotenv()
    pagina = 1
    itens = 100
    deputados = []
    acabou = False
    api = f'{os.getenv("CAMARA_API")}deputados?pagina={pagina}&itens={itens}&datainicio=2000-01-01&datafim=3000-01-01'
    while not acabou:
        data = requests.get(api).json()
        tem_next = False
        for link in data["links"]:
            if link["rel"] == "next":
              api = link["href"]
              tem_next = True
        for deputado in data["dados"]:
            print("Pegando da api dados do deputado:", deputado["nome"])
            deputados.append(deputado)
        if not tem_next:
            acabou = True
    return deputados


def salvar_deputados(deputados: list):
    con = database.conectar()
    if not con[0]:
        return{
            "status": con[0],
            "message": con[1]
        }
    con = con[1]
    for deputado in deputados:
        api = f"{os.getenv('CAMARA_API')}deputados/{deputado['id']}"
        dep = requests.get(api).json()
        deputado = dep["dados"]
        print(f"Inserindo/atualizando deputado {deputado['nomeCivil']}")
        sql = '''
            INSERT INTO deputado VALUES(
            uuid_generate_v4(),
            {id_camara},
            {nome_civil},
            {nome},
            {sigla_partido},
            {sigla_uf},
            {email},
            {gabinente_nome},
            {gabinete_predio},
            {gabinete_sala},
            {gabinete_andar},
            {gabinete_telefone},
            {situacao},
            {sexo},
            {data_nascimento},
            {data_falecimento},
            {municipio_nascimento},
            {escolaridade},
            current_timestamp,
            current_timestamp
            )on conflict(id_camara) do update
                set sigla_partido = {sigla_partido},
                sigla_uf = {sigla_uf},
                email = {email},
                gabinete_nome = {gabinente_nome},
                gabinete_predio = {gabinete_predio},
                gabinete_sala = {gabinete_sala},
                gabinete_andar = {gabinete_andar},
                gabinete_telefone = {gabinete_telefone},
                situacao = {situacao},
                data_falecimento = {data_falecimento},
                escolaridade = {escolaridade},
                data_atualizacao = current_timestamp
        '''.format(
            id_camara=common.verifica_nulo(deputado["id"]),
            nome_civil=common.verifica_nulo(deputado["nomeCivil"].upper()),
            nome=common.verifica_nulo(deputado["ultimoStatus"]["nome"].upper()),
            sigla_partido=common.verifica_nulo(deputado["ultimoStatus"]["siglaPartido"].upper()),
            sigla_uf=common.verifica_nulo(deputado["ultimoStatus"]["siglaUf"].upper()),
            email=f''' '{deputado["ultimoStatus"]["email"]}' ''' if deputado["ultimoStatus"]["email"] is not None else 'null',
            gabinente_nome=common.verifica_nulo(deputado["ultimoStatus"]["gabinete"]["nome"].upper()),
            gabinete_predio=common.verifica_nulo(deputado["ultimoStatus"]["gabinete"]["predio"]),
            gabinete_sala=common.verifica_nulo(deputado["ultimoStatus"]["gabinete"]["sala"]),
            gabinete_andar=common.verifica_nulo(deputado["ultimoStatus"]["gabinete"]["andar"]),
            gabinete_telefone=common.verifica_nulo(deputado["ultimoStatus"]["gabinete"]["telefone"].replace("-", "")),
            situacao=common.verifica_nulo(deputado["ultimoStatus"]["situacao"].upper()),
            sexo=common.verifica_nulo(deputado["sexo"].upper()),
            data_nascimento=common.verifica_nulo(deputado["dataNascimento"]),
            data_falecimento=common.verifica_nulo(deputado["dataFalecimento"]),
            municipio_nascimento=common.verifica_nulo(deputado["municipioNascimento"].upper()),
            escolaridade=common.verifica_nulo(deputado["escolaridade"].upper())
        )
        with con.cursor() as cursor:
            try:
                cursor.execute(sql)
                con.commit()
            except Exception as e:
                print(f"Erro ao inserir deputado {deputado['nomeCivil']}")


def get_despesas():
    dotenv.load_dotenv()
    pagina = 1
    itens = 100
    despesas = []
    acabou = False
    api = f'{os.getenv("CAMARA_API")}deputados/'
    con = database.conectar()
    if not con[0]:
        return {
            "status": con[0],
            "message": con[1]
        }
    con = con[1]
    with con.cursor(cursor_factory=RealDictCursor) as cursor:
        sql = "select id_camara, nome, _id from deputado order by nome asc"
        cursor.execute(sql)
        deputados = cursor.fetchall()
        for deputado in deputados:
            hoje = datetime.date.today()
            #primeiro_ano = hoje - datetime.timedelta(weeks=53)
            primeiro_ano = hoje - datetime.timedelta(weeks=4)
            primeiro_ano = primeiro_ano.year
            ano_string = f"?ano={primeiro_ano}"
            primeiro_ano += 1
            while primeiro_ano <= hoje.year:
                ano_string = f"{ano_string}&ano={primeiro_ano}"
                primeiro_ano += 1
            print(f"Selecionando despesas do deputado {deputado['nome']}")
            api_despesa = f"{api}{deputado['id_camara']}/despesas{ano_string}&itens=100"
            acabou = False
            pag_anterior = 0
            pag_seguinte = 1
            while not acabou:
                print('            pagina:',pag_seguinte)
                data = requests.get(api_despesa).json()
                pag_anterior = pag_seguinte
                tem_next = False
                last = ""
                atual = ""
                for link in data["links"]:
                    if link["rel"] == "next":
                        api_despesa = link["href"]
                        pag_seguinte += 1
                    if link["rel"] == "last":
                        last = link["href"]
                if last == api_despesa or pag_seguinte == pag_anterior:
                    tem_next = False
                else:
                    tem_next = True
                for despesa in data["dados"]:
                    despesa = dict(despesa)
                    despesa["deputado_id"] = deputado["_id"]
                    despesas.append(despesa)
                if not tem_next:
                    acabou = True
    return despesas


def salvar_despesas(despesas):
    con = database.conectar()
    if not con[0]:
        return {
            "status": con[0],
            "message": con[1]
        }
    con = con[1]
    for despesa in despesas:
        print(f"                  ...cadastrando/atualizando despesa {despesa['ano']}/{despesa['mes']} - {despesa['numDocumento']}")
        sql = '''
            insert into despesas values(
                uuid_generate_v4(),
                '{deputado_id}',
                {ano},
                {mes},
                {tipo_despesa},
                {tipo_documento},
                {data_documento},
                {documento},
                {valor_documento},
                {url_documento},
                {fornecedor_cnpjcpf},
                {fornecedor_nome},
                {valor_liquido},
                {valor_glosa},
                {codigo_lote},
                current_timestamp,
                current_timestamp
            )on conflict(deputado_id, ano, mes, documento, fornecedor_cnpjcpf) do update
                set
                    ano={ano},
                    mes={mes},
                    tipo_despesa={tipo_despesa},
                    tipo_documento={tipo_documento},
                    data_documento={data_documento},
                    valor_documento={valor_documento},
                    url_documento={url_documento},
                    fornecedor_cnpjcpf={fornecedor_cnpjcpf},
                    fornecedor_nome={fornecedor_nome},
                    valor_liquido={valor_liquido},
                    valor_glosa={valor_glosa},
                    codigo_lote={codigo_lote},
                    data_atualizacao = current_timestamp
        '''.format(
            deputado_id=despesa["deputado_id"],
            ano=common.verifica_nulo(despesa["ano"]),
            mes=common.verifica_nulo(despesa["mes"]),
            tipo_despesa=common.verifica_nulo(despesa["tipoDespesa"]),
            tipo_documento=common.verifica_nulo(despesa["tipoDocumento"]),
            data_documento=common.verifica_nulo(despesa["dataDocumento"]),
            documento=common.verifica_nulo(despesa['numDocumento']),
            valor_documento=common.verifica_nulo(despesa["valorDocumento"]),
            url_documento=f''' '{despesa["urlDocumento"]}' ''' if despesa["urlDocumento"] is not None else "null",
            fornecedor_cnpjcpf=common.verifica_nulo(despesa["cnpjCpfFornecedor"]),
            fornecedor_nome=common.verifica_nulo(despesa["nomeFornecedor"]),
            valor_liquido=common.verifica_nulo(despesa["valorLiquido"]),
            valor_glosa=common.verifica_nulo(despesa["valorGlosa"]),
            codigo_lote=common.verifica_nulo(despesa["codLote"])
        )
        with con.cursor() as cursor:
            cursor.execute(sql)
            con.commit()

