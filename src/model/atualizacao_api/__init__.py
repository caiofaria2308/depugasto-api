from typing import Optional, List
from pydantic import BaseModel

class AtualizacaoApi(BaseModel):
    id: str
    data_atualizacao: str


class Response(BaseModel):
    status: bool
    data: Optional[List[AtualizacaoApi]]
    message: Optional[str]
    quantidade_total: Optional[int]
    quantidade_por_pagina: Optional[int]
    quantidade_de_pagina: Optional[int]
    pagina: Optional[int]