from typing import Optional, List
from pydantic import BaseModel

class Deputado(BaseModel):
    id: str
    id_camara: int
    nome_civil: str
    nome: str
    sigla_partido: Optional[str]
    sigla_uf: Optional[str]
    email: Optional[str]
    gabinete_nome: Optional[str]
    gabinete_predio: Optional[str]
    gabinete_sala: Optional[str]
    gabinete_andar: Optional[str]
    gabinete_telefone: Optional[str]
    situacao: Optional[str]
    sexo: Optional[str]
    data_nascimento: Optional[str]
    data_falecimento: Optional[str]
    municipio_falecimento: Optional[str]
    escolaridade: Optional[str]
    data_criacao: str
    data_atualizacao: str



class Response(BaseModel):
    status: bool
    data: Optional[List[Deputado]]
    message: Optional[str]
    quantidade_total: Optional[int]
    quantidade_por_pagina: Optional[int]
    quantidade_de_pagina: Optional[int]
    pagina: Optional[int]
