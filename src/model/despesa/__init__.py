from typing import Optional, List
from pydantic import BaseModel
import datetime


class Despesa(BaseModel):
    id: str
    deputado_id: str
    ano: int
    mes: int
    tipo_despesa: str
    tipo_documento: str
    documento: str
    valor_documento: float
    url_documento: Optional[str]
    fornecedor_cnpjcpf: str
    fornecedor_nome: str
    valor_liquido: float
    valor_glosa: float
    codigo_lote: Optional[int]
    data_criacao: str
    data_atualizacao: str



class ResponseDespesa(BaseModel):
    status: bool
    data: Optional[List[Despesa]]
    message: Optional[str]
    quantidade_total: Optional[int]
    quantidade_por_pagina: Optional[int]
    quantidade_de_pagina: Optional[int]
    pagina: Optional[int]


class DespesaRank(BaseModel):
    deputado_id: str
    deputado_nome: str
    deputado_id_camara: str
    total_gasto: float


class ResponseDespesaRank(BaseModel):
    status: bool
    data: Optional[List[DespesaRank]]
    message: Optional[str]


class DespesaDeputadoRank(BaseModel):
    tipo_despesa: str
    total_liquido: float
    total_glosa: float



class ResponseDespesaDeputadoRank(BaseModel):
    status: bool
    data: Optional[List[DespesaDeputadoRank]]
    message: Optional[str]
