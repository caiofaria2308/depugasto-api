import datetime
import re


def verifica_nulo(valor) -> str:
    if type(valor) == str:
        valor = re.sub(r'\W+', ' ', valor)
        return f"'{valor}'" if valor is not None else "null"
    elif type(valor) == datetime:
        return f"'{str(valor)}'" if valor is not None else "null"
    else:
        return f"{valor}" if valor is not None else "null"