from typing import Tuple, Optional
import os
import psycopg2
import dotenv

def conectar() -> Tuple[bool, Optional[psycopg2.connect]]:
    try:
        dotenv.load_dotenv()
        conn = psycopg2.connect(
            f'''
                dbname='{os.getenv("DATABASE_NAME")}'
                user='{os.getenv("DATABASE_USER")}'
                host='{os.getenv("DATABASE_HOST")}'
                password='{os.getenv("DATABASE_PASSWORD")}'
                '''
        )
        return True, conn
    except Exception as e:
        print("Erro ao conectar com banco:", str(e))
        return False, None