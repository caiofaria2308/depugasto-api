from fastapi import FastAPI
from typing import Optional
import src.model.atualizacao_api as model
import src.controller.atualizacao_api as controller

def rotas(app: FastAPI, prefixo: str) ->None:
    @app.get(f"{prefixo}atualizacao-api", response_model=model.Response)
    async def get_atualizacoes(pagina: Optional[int] = 1, qtd_pagina: Optional[int] = 20):
        return controller.get_all(pagina=pagina, quantidade_pagina=qtd_pagina)
    return
