from fastapi import FastAPI
from typing import Optional
import src.model.despesa as model
import src.controller.despesa as controller

def rotas(app: FastAPI, prefixo: str) ->None:
    @app.get(f"{prefixo}despesas", response_model=model.ResponseDespesa)
    async def get_despesas(pagina: Optional[int] = 1, qtd_pagina: Optional[int] = 20, ano: Optional[int] = 2021):
        return controller.get_all(pagina=pagina, quantidade_pagina=qtd_pagina, ano=ano)

    @app.get(f"{prefixo}rank-despesas", response_model=model.ResponseDespesaRank)
    def get_rank_despesas(ano: Optional[int] = None):
        return controller.rank_despesas(ano=ano)

    @app.get(prefixo+"despesas/deputado", response_model=model.ResponseDespesa)
    def get_despesas_deputado(deputado_id: str, pagina: Optional[int] = 1, qtd_pagina: Optional[int] = 20, ano: Optional[int] = None):
        return controller.get_despesas_deputado(
            deputado_id=deputado_id,
            pagina=pagina,
            quantidade_pagina=qtd_pagina,
            ano=ano
        )

    @app.get(prefixo+"rank-despesa-deputado", response_model=model.ResponseDespesaDeputadoRank)
    def get_rank_despesas_deputado(deputado_id: str, ano: Optional[int] = None):
        return controller.get_rank_despesa_deputado(deputado_id=deputado_id, ano=ano)
    return
