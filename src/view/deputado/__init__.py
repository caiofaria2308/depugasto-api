from fastapi import FastAPI
from typing import Optional
import src.model.deputado as model
import src.controller.deputado as controller

def rotas(app: FastAPI, prefixo: str) ->None:
    @app.get(f"{prefixo}deputado", response_model=model.Response)
    async def get_deputados(pagina: Optional[int] = 1, qtd_pagina: Optional[int] = 20, uf: Optional[str] = "SP" ):
        return controller.get_all(pagina=pagina, quantidade_pagina=qtd_pagina, uf=uf.upper())
    return
